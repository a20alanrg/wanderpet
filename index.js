const sqlite3 = require('sqlite3').verbose();
const xmlbuilder = require('xmlbuilder');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const multer = require('multer');

const port = 4000;

// Ruta donde se encuentra la base de datos SQLite
const rutaBaseDatos = 'public/db/PetWatchDB.db';

// Crear una nueva conexión a la base de datos SQLite
const db = new sqlite3.Database(rutaBaseDatos, (error) => {
  if (error) {
    console.error('Error al abrir la base de datos', error.message);
  } else {
    console.log('Conexión a la base de datos SQLite establecida');
  }
});



// Endpoint para obtener datos de la tabla Publicacion
app.get('/api/Usuario', (req, res) => {
  const consulta = 'SELECT * FROM Usuario';  // Asegúrate de que la tabla 'Publicacion' existe en tu base de datos
  db.all(consulta, (error, filas) => {
    if (error) {
      console.error('Error al obtener datos de la base de datos:', error.message);
      res.status(500).json({ error: 'Error al obtener datos de la base de datos' });
    } else {
      res.json(filas);
    }
  });
});

app.get('/api/Publicacion', (req, res) => {
  const consulta2 = 'SELECT * FROM Publicacion ORDER BY Id_Publicacion DESC';  // Asegúrate de que la tabla 'Publicacion' existe en tu base de datos
  db.all(consulta2, (error, filas) => {
    if (error) {
      console.error('Error al obtener datos de la base de datos:', error.message);
      res.status(500).json({ error: 'Error al obtener datos de la base de datos' });
    } else {
      res.json(filas);
    }
  });
});

// Definir la ruta de la API
app.get('/api/Comentario', (req, res) => {
  const id = req.query.id; // Obtener el ID de la consulta
  
  // Consulta SQL para seleccionar publicaciones filtradas por ID
  const sql = `SELECT * FROM Comentario WHERE Id_Publicacion = ? ORDER BY Id_Comentario DESC`;

  // Ejecutar la consulta con el ID proporcionado
  db.all(sql, [id], (err, results) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      res.status(500).json({ error: 'Error al obtener las publicaciones del servidor' });
      return;
    }
    res.json(results); // Devolver los resultados de la consulta como JSON
  });
});

app.get('/api/Perfil', (req, res) => {
  const id = req.query.id; // Obtener el ID de la consulta
  
  // Consulta SQL para seleccionar publicaciones filtradas por ID
  const sql = `SELECT * FROM Publicacion WHERE Id_Usuario = ?`;

  // Ejecutar la consulta con el ID proporcionado
  db.all(sql, [id], (err, results) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      res.status(500).json({ error: 'Error al obtener las publicaciones del servidor' });
      return;
    }
    res.json(results); // Devolver los resultados de la consulta como JSON
  });
});

app.get('/api/PubliUser', (req, res) => {
  const id = req.query.id; // Obtener el ID de la consulta
  
  // Consulta SQL para seleccionar publicaciones filtradas por ID
  const sql = `SELECT * FROM Usuario WHERE Id_Usuario = ?`;

  // Ejecutar la consulta con el ID proporcionado
  db.all(sql, [id], (err, results) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      res.status(500).json({ error: 'Error al obtener las publicaciones del servidor' });
      return;
    }
    res.json(results); // Devolver los resultados de la consulta como JSON
  });
});

app.get('/api/Buscar', (req, res) => {
  const lugar = req.query.lugar; // Obtener el ID de la consulta
  
  // Consulta SQL para seleccionar publicaciones filtradas por ID
  const sql = `SELECT * FROM Publicacion WHERE Localizacion LIKE ? ORDER BY Id_Publicacion DESC`;

  // Ejecutar la consulta con el lugar proporcionado
  db.all(sql, [`%${lugar}%`], (err, results) => {
    if (err) {
      console.error('Error al ejecutar la consulta:', err);
      res.status(500).json({ error: 'Error al obtener las publicaciones del servidor' });
      return;
    }
    res.json(results); // Devolver los resultados de la consulta como JSON
  });
});


app.get('/api/logout', (req, res) => {
  const fs = require('fs');

  // Ruta del archivo XML vacío
  const filePath = 'public/db/usuario.xml';

  // Contenido del archivo XML vacío
  const xmlContent = '<?xml version="1.0" encoding="UTF-8"?>\n';

  // Escribe el contenido en el archivo
  fs.writeFile(filePath, xmlContent, (err) => {
    if (err) {
      console.error('Error al escribir en el archivo:', err);
      res.status(500).send('Error al borrar datos en el archivo XML');
    }else {
      console.log('Archivo XML vacío creado correctamente.');
      res.status(200).send('Datos borrados en el archivo XML correctamente');
    }
    
  });
});




// Middleware para parsear JSON y URL-encoded bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Servir archivos estáticos (tu página HTML)
const publicPath = path.join(__dirname, 'public');
app.use(express.static(publicPath));


app.get('/api/buscar', (req, res) => {
  console.log(req.body)
  const { lugar } = req.body; // Obtener 'expresion' en lugar de 'lugar'
  console.log(lugar);

  if (!lugar) {
      return res.status(400).json({ error: 'Expression is required' });
  }

  const sqlQuery = `SELECT * FROM Publicacion WHERE Localizacion LIKE '%${lugar}%'`;

  // Ejecutar la consulta
  db.all(sqlQuery, (err, rows) => {
      if (err) {
          console.error('Error al ejecutar la consulta:', err);
          res.status(500).json({ error: 'Error al ejecutar la consulta en la base de datos' });
      } else {
          res.json(rows); // Enviar los resultados como respuesta
      }
  });
});


// Endpoint para insertar un usuario
app.post('/api/NewUsuario', (req, res) => {
  const { nickname, psdw } = req.body;

  if (!nickname || !psdw) {
    return res.status(400).json({ error: 'Faltan datos del usuario' });
  }

  const insertQuery = 'INSERT INTO Usuario (Nickname, Contraseña) VALUES (?, ?)';
  db.run(insertQuery, [nickname, psdw], function(error) {
    if (error) {
      console.error('Error al insertar datos en la base de datos:', error.message);
      return res.status(500).json({ error: 'Error al insertar datos en la base de datos' });
    }

    res.status(201).json({ message: 'Usuario registrado correctamente', id: this.lastID });
  });
});

app.post('/api/NewComentario', (req, res) => {
  const { mensaje, id_publi, nickname } = req.body;

  const insertQuery = 'INSERT INTO Comentario (Mensaje, Id_Publicacion, Nickname) VALUES (?, ?, ?)';
  db.run(insertQuery, [mensaje, id_publi, nickname], function(error) {
    if (error) {
      console.error('Error al insertar datos en la base de datos:', error.message);
      return res.status(500).json({ error: 'Error al insertar datos en la base de datos' });
    }

    res.status(201).json({ message: 'Comentario subido correctamente', id: this.lastID });
  });
});

app.post('/api/saveUser', (req, res) => {
  const { nickname } = req.body;
  const consulta = 'SELECT * FROM Usuario WHERE Nickname = ?';

  db.all(consulta, [nickname], (error, filas) => {
    if (error) {
      console.error('Error al ejecutar la consulta SELECT', error.message);
      res.status(500).send('Error al obtener datos de la base de datos');
    } else {
      if (filas.length === 0) {
        res.status(404).send('No se encontraron usuarios con ese nombre');
        return;
      }

      // Crear XML
      const root = xmlbuilder.create('usuarios');
      filas.forEach((fila) => {
        const usuario = root.ele('usuario');
        for (const columna in fila) {
          if (columna!="Contraseña") {
            usuario.ele(columna, fila[columna]);
          }
          
        }
      });

      const xmlString = root.end({ pretty: true });
      const rutaArchivoXML = path.join(__dirname, 'public/db/usuario.xml');

      // Escribir XML en el archivo
      fs.writeFile(rutaArchivoXML, xmlString, (error) => {
        if (error) {
          console.error('Error al escribir el archivo XML', error.message);
          res.status(500).send('Error al guardar datos en el archivo XML');
        } else {
          console.log('Datos guardados en el archivo XML correctamente');
          res.status(200).send('Datos guardados en el archivo XML correctamente');
        }
      });
    }
  });
});

app.post('/api/savePubli', (req, res) => {
  const { id } = req.body;
  const consulta = 'SELECT * FROM Publicacion WHERE Id_Publicacion = ?';

  db.all(consulta, [id], (error, filas) => {
    if (error) {
      console.error('Error al ejecutar la consulta SELECT', error.message);
      res.status(500).send('Error al obtener datos de la base de datos');
    } else {
      if (filas.length === 0) {
        res.status(404).send('No se encontraron publicaciones con ese nombre');
        return;
      }

      // Crear XML
      const root = xmlbuilder.create('publicaciones');
      filas.forEach((fila) => {
        const usuario = root.ele('publicacion');
        for (const columna in fila) {
          usuario.ele(columna, fila[columna]);
        }
      });

      const xmlString = root.end({ pretty: true });
      const rutaArchivoXML = path.join(__dirname, 'public/db/publicacion.xml');

      // Escribir XML en el archivo
      fs.writeFile(rutaArchivoXML, xmlString, (error) => {
        if (error) {
          console.error('Error al escribir el archivo XML', error.message);
          res.status(500).send('Error al guardar datos en el archivo XML');
        } else {
          res.status(200).json({ message: 'Publicación guardada correctamente' });
        }
      });
    }
  });
});

app.delete('/deletePubli', async (req, res) => {
  const id = req.query.id;

  const sql = 'DELETE FROM Publicacion WHERE Id_Publicacion = ?';

  db.run(sql, [id], function(error) {
    if (error) {
      console.error('Error al eliminar el elemento:', error.message);
    } else {
      console.log(`Elemento con ID ${id} eliminado exitosamente`);
    }
  });
});

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/img/') // Carpeta donde se guardarán las imágenes
  },
  filename: function (req, file, cb) {
    // Conservar la extensión original del archivo
    cb(null, file.originalname); 
  }
});
const upload = multer({ storage: storage });

app.post('/upload', upload.single('imagen'), (req, res) => {
  const {nombre,localizacion,descrip,id_Usuario} = req.body;
  const path=req.file.path;
  const Xpath=path.substring(7);

  const sql = 'INSERT INTO Publicacion (Nombre_mascota, Localizacion, Descripcion, Id_Usuario, Imagen) VALUES (?, ?, ?, ?, ?)';
  db.run(sql, [nombre, localizacion,descrip,id_Usuario,Xpath], (error) => {
    if (error) {
      console.error('Error al insertar datos en la base de datos:', error.message);
      res.status(500).json({ error: 'Error al insertar datos en la base de datos' });
    } else {
      console.log('Datos insertados correctamente en la base de datos');
      res.status(200).json({ message: 'Publicación creada exitosamente' });
    }
  });
});

app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});


